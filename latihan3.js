// HelloWorld.js
const http = require("http");
const url = require("url");

const server = http.createServer((request, response) => {
  const parsedURL = url.parse(request.url, true);
  console.log(parsedURL);

  if (parsedURL.pathname == "/") {
    // HTTP Status: 200 : OK
    response.statusCode = 200;
    response.setHeader("Content-Type", "text/html");
    response.write("<h1>Ini Halaman Beranda </h1>");
    response.write(`<a href="profil?nama=Dian Olivia&nim=105841107919">Dian Olivia</a></br>`);
    response.write(
      `<a href="profil?nama=Fajar Maulana&nim=105841107419">Fajar Maulana</a></br>`
    );
    response.write(`<a href="profil?nama=Hardiyanti&nim=105841108319">Hardiyanti</a></br>`);
    response.write(`<a href="profil?nama=Isna&nim=105841110319">Isna</a></br>`);
    response.write(`<a href="profil?nama=Kamran&nim=105841109319">Kamran</a></br>`);

    response.end();
  } else if (parsedURL.pathname == "/profil") {
    // HTTP Status: 200 : OK
    response.statusCode = 200;
    response.write(`<h1>Ini Halaman Profil ${parsedURL.query.nama} </h1>`);
    response.write(`<h1>NIM : ${parsedURL.query.nim} </h1>`);
    response.write(`<h1>Nama : ${parsedURL.query.nama} </h1>`);
    response.write(`<img src="https://simak.unismuh.ac.id/upload/mahasiswa/${parsedURL.query.nim}.jpg" alt="${parsedURL.query.nama}">
    `);
    response.end();
  } else {
    response.statusCode = 404;
    response.write("<h1>Halaman " + request.url + " tidak ditemukan</h1>");
    response.write("<h1>Si Client meminta bahasa</h1>");
    response.write(request.headers["accept-language"]);
    response.end();
  }
});

server.listen(4003, () => {
  // Console will print the message
  console.log("Server running at  http://140.238.201.32:4003/");
});
