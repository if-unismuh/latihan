// HelloWorld.js
const http = require("http");

const server = http.createServer((request, response) => {
  console.log(request.headers['accept-language']);

  if (request.url === "/") {
    // Send the HTTP header
    // HTTP Status: 200 : OK
    response.statusCode = 200;
    // Content Type: text/plain
    response.setHeader("Content-Type", "text/html");

  
    // Send the response body as "Hello World"
    // const head = request.rawHeaders
    response.write('Ini Adalah Halaman <b>Beranda</b> </br>');
    response.write(`'Bahasa' ${request.headers['accept-language']} "URL": ${request.url} </br>`);

    response.write("<a href='/profile'>Profile</a></br>");
    response.write("<a href='/setting'>Setting</a></br>");
    response.end();
  } 
  else if (request.url === "/profile") {
   // Send the HTTP header
   // HTTP Status: 200 : OK
   response.statusCode = 200;
   // Content Type: text/plain
   response.setHeader("Content-Type", "text/html");
   // Send the response body as "Hello World"
   // const head = request.rawHeaders
   response.write("Halaman Profil <b>Resky</b> </br>");
   response.write("NIM : 10 </br>");
   response.write("Nama : Resky Agus </br>");
   response.write("<a href='/'>Beranda</a></br>");
   response.end();
 }
  else if (request.url === "/setting") {
   // Send the HTTP header
   // HTTP Status: 200 : OK
   response.statusCode = 200;
   // Content Type: text/plain
   response.setHeader("Content-Type", "text/html");
   // Send the response body as "Hello World"
   // const head = request.rawHeaders
   response.write("Halaman  <b>Setting</b> </br>");
   response.write("<a href='/'>Beranda</a></br>");
   
   response.end();
 }
 else {
      // Send the HTTP header
   // HTTP Status: 200 : OK
   response.statusCode = 404;
   // Content Type: text/plain
   response.setHeader("Content-Type", "text/html");
   // Send the response body as "Hello World"
   // const head = request.rawHeaders
   response.write("Halaman  <b>Tidak ditemukan</b> </br>");
   response.write("<a href='/'>Beranda</a></br>");
   
   response.end();

 }

});

server.listen(4002, () => {
  // Console will print the message
  console.log("Server running at  http://140.238.201.32:4002/");
});
