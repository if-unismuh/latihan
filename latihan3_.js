// HelloWorld.js
const http = require("http");
const url = require("url");

const server = http.createServer((request, response) => {
  const parsedURL = url.parse(request.url, true);
  // console.log(parsedURL)
  console.log(parsedURL);

  // http://localhost:4001/profile?nama=Nabil&nim=123456
  // nodemon  >> sudo npm install nodemon -g -- yarn install nodemon

  if (parsedURL.pathname == "/") {
    // HTTP Status: 200 : OK
    response.statusCode = 200;
    // Content Type: text/plain
    response.setHeader("Content-Type", "text/html");
    response.write("<h1>Ini Halaman Beranda </h1>");
    // Send the HTTP header
    // HTTP Status: 200 : OK
    
    response.write(`<a href="/profil?nama=Nabil">Nabil</a><br>`);
    response.write(`<a href="/profil?nama=Nur Alam">Nur Alam</a><br>`);
    response.write(`<a href="/profil?nama=Mujadila">Mujadila</a><br>`);
    response.end();
  } else if (parsedURL.pathname == "/profil") {
    // HTTP Status: 200 : OK
    response.statusCode = 200;
    response.setHeader("Content-Type", "text/html");
    response.write(`<h1>Ini Halaman Profil http://simak.dsdsdsd/${parsedURL.query.nim}.jpg  </h1>`);
    response.end();
  }
  else {
   response.statusCode = 404;
   response.write('<h1>Halaman '+request.url+' tidak ditemukan</h1>');
   response.write('<h1>Si Client meminta bahasa</h1>');
   response.write(request.headers['accept-language']);
   response.end();
  }


});

server.listen(4001, () => {
  // Console will print the message
  console.log("Server running at  http://140.238.201.32:4001/");
});
