// HelloWorld.js
const http = require("http");
const mariadb = require("mariadb");

// const conn = mariadb.createPool({
// const conn = mariadb.createConnection({
//   host: "db2019.if.unismuh.ac.id",
//   port: "3318",
//   user: "root",
//   password: "if2019",
//   database: "latihan",
//   connectionLimit: 5,
// });

// const conn = mariadb.createConnection('mariadb://root:if2019@db2019.if.unismuh.ac.id:3318/latihan');
// const conn =  mariadb.createConnection('mariadb://root:if2019@db2019.if.unismuh.ac.id:3318/latihan');

const getConnect = async () => {
   try {
      const conn =  await mariadb.createConnection('mariadb://root:if2019@db2019.if.unismuh.ac.id:3318/latihan');
      return conn
      
   } catch (error) {
      return error
   }

}

const mahasiswaFindAll = async () => {
   

  try {
  //  const conn =  await mariadb.createConnection('mariadb://root1:if2019@db2019.if.unismuh.ac.id:3318/latihan');
  const conn = await getConnect()
   const rows = await conn.query(`SELECT * FROM mahasiswa LIMIT 10`);

    return rows;
  } catch (error) {
    console.log(error);
    return error;
  } 
};

const mahasiswaFindOne = async (nim) => {
   try {
      // const conn =  await mariadb.createConnection('mariadb://root:if2019@db2019.if.unismuh.ac.id:3318/latihan');
      const conn = await getConnect()
      const rows = await conn.query('SELECT * FROM mahasiswa WHERE nim=?', [nim]);
     console.log(rows[0]);
     return rows[0];
   } catch (error) {
     console.log(error);
   //   if (conn) conn.release();
     return error;
   } finally {
   //   if (conn) conn.release(); //release to pool
   }
 };

const server = http.createServer(async (request, response) => {
  const mahasiswa = await mahasiswaFindAll();
//   const mahasiswa = await mahasiswaFindOne('1058411004191');
  // const mahasiswa = await mahasiswaFindOne('105841100419');
   // console.log(mahasiswa)


  // Send the HTTP header
  // HTTP Status: 200 : OK
  response.statusCode = 200;
  // Content Type: text/plain
  response.setHeader("Content-Type", "text/plain");
  // Send the response body as "Hello World"
  // const head = request.rawHeaders
  // response.write(`Hello ${mahasiswa.nama} (${mahasiswa.nim})`);
  mahasiswa.map((elemen) => {
    response.write(`\nNIM: ${elemen.nim}`);
    response.write(`\nNama: ${elemen.nama}`);
  });

  response.end();
});

server.listen(4004, () => {
  // Console will print the message
  console.log("Server running at  http://140.238.201.32:4004/");
});
