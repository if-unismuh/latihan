// HelloWorld.js
const http = require("http");
const url = require("url");

const server = http.createServer((request, response) => {
  const parsedURL = url.parse(request.url, true);
  console.log(parsedURL);
  

  if (parsedURL.pathname === "/") {
    
    // Send the HTTP header
    // HTTP Status: 200 : OK
    response.statusCode = 200;
    // Content Type: text/plain
    response.setHeader("Content-Type", "text/html");

  
    // Send the response body as "Hello World"
    // const head = request.rawHeaders
    response.write('Ini Adalah Halaman <b>Beranda</b> </br>');
    
    response.write(`<a href='/profile?nim=105841102719&nama=RESKY AGUS J'>Profile - RESKY AGUS J</a></br>`);
    response.write(`<a href='/profile?nim=105841100719&nama=ANDI MUHARRAM'>Profile - ANDI MUHARRAM</a></br>`);
    response.write(`<a href='/profile?nim=105841103419&nama=SITI HAJAR HAMZAH'>Profile - SITI HAJAR HAMZAH	</a></br>`);
    response.write("<a href='/setting'>Setting</a></br>");
    response.end();
  } 
  else if (parsedURL.pathname === "/profile") {
   // Send the HTTP header
   // HTTP Status: 200 : OK
   response.statusCode = 200;
   // Content Type: text/plain
   response.setHeader("Content-Type", "text/html");
   // Send the response body as "Hello World"
   // const head = request.rawHeaders
   response.write(`Halaman Profil <b>${parsedURL.query.nama}</b> </br>`);
   response.write(`<img src="https://simak.unismuh.ac.id/upload/mahasiswa/${parsedURL.query.nim}.jpg" alt="Italian Trulli"></img>`)
   response.write(`</br>NIM : ${parsedURL.query.nim} </br>`);
   response.write(`Nama : ${parsedURL.query.nama} </br>`);
   response.write("<a href='/'>Beranda</a></br>");
   response.end();
 }
  else if (parsedURL.pathname === "/setting") {
   // Send the HTTP header
   // HTTP Status: 200 : OK
   response.statusCode = 200;
   // Content Type: text/plain
   response.setHeader("Content-Type", "text/html");
   // Send the response body as "Hello World"
   // const head = request.rawHeaders
   response.write("Halaman  <b>Setting</b> </br>");
   response.write("<a href='/'>Beranda</a></br>");
   
   response.end();
 }
 else {
      // Send the HTTP header
   // HTTP Status: 200 : OK
   response.statusCode = 404;
   // Content Type: text/plain
   response.setHeader("Content-Type", "text/html");
   // Send the response body as "Hello World"
   // const head = request.rawHeaders
   response.write("Halaman  <b>Tidak ditemukan</b> </br>");
   response.write("<a href='/'>Beranda</a></br>");
   
   response.end();

 }

});

server.listen(4002, () => {
  // Console will print the message
  console.log("Server running at  http://140.238.201.32:4002/");
});
