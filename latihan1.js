// HelloWorld.js
const http = require("http");

const server = http.createServer((request, response) => {
   // Send the HTTP header 
   // HTTP Status: 200 : OK
   response.statusCode = 200;
   // Content Type: text/plain
   response.setHeader('Content-Type', 'text/plain');
   // Send the response body as "Hello World"
   // const head = request.rawHeaders
   response.write('Hello World');
   response.end();
});

server.listen(4001, () => {
   // Console will print the message
   console.log('Server running at  http://140.238.201.32:4001/');
})
