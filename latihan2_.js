// HelloWorld.js
const http = require("http");

const server = http.createServer((request, response) => {
   // Send the HTTP header 
  
   // Content Type: text/plain
   response.setHeader('Content-Type', 'text/html');
   // Send the response body as "Hello World"
   // const head = request.rawHeaders
   if(request.url=='/'){
       // HTTP Status: 200 : OK
      response.statusCode = 200;
      response.write('<h1>Ini Halaman Beranda </h1>');
      
      response.end();
   }
   else if(request.url=='/profil'){
       // HTTP Status: 200 : OK
      response.statusCode = 200;
      response.write('<h1>Ini Halaman Profil </h1>');
      response.end();
   }
   else if(request.url=='/krs'){
      // HTTP Status: 200 : OK
     response.statusCode = 200;
     response.write('<h1>Ini Halaman KRS </h1>');
     response.end();
  }
  else {
   response.statusCode = 404;
   response.write('<h1>Halaman '+request.url+' tidak ditemukan</h1>');
   response.write('<h1>Si Client meminta bahasa</h1>');
   response.write(request.headers['accept-language']);
   response.end();

  }

  
   

   console.log(request.headers)

});

server.listen(4002, () => {
   // Console will print the message
   console.log('Server running at  http://ip-public-anda-disini:4002/');
})
